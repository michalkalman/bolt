module.exports = (TrackRepository) => {
    async function Execute(track) {
        return new Promise((resolve, reject) => {
            TrackRepository.save(track)
                .then((trackSave) => {
                    TrackRepository.getActivityForTrack(trackSave)
                        .then((activity) => {
                            resolve(activity);
                        }, (err) => {
                            reject(err);
                        });
                });
        });
    }

    return {
        Execute,
    };
};
