module.exports = (TrackRepository) => {
    async function Execute(activity) {
        return new Promise((resolve, reject) => {
            TrackRepository.createActivity(activity)
                .then((response) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    return {
        Execute,
    };
};
