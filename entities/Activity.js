module.exports = class Activity {
    constructor(track, type, uri) {
        this.track = track;
        this.type = type;
        this.uri = uri;
    }
};
