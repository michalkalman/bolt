const InMemoryDatabaseServices = require('../frameworks/persistance/InMemoryDatabaseServices');
// const SqliteDatabaseServices = require('../frameworks/persistance/SqliteDatabaseServices');
const ZmqMessageService = require('../frameworks/messaging/ZmqMessageService');

module.exports = {
    DatabaseService: new InMemoryDatabaseServices(),
    // DatabaseService: new SqliteDatabaseServices(),
    MessageService: new ZmqMessageService(),
};
