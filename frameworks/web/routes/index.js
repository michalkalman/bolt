const express = require('express');
const TrackController = require('../../../controllers/TrackController');

const apiRouter = (dependencies) => {
    const router = express.Router();
    const controller = TrackController(dependencies);

    router.route('/:track')
        .get(controller.processTrack);

    return router;
};

module.exports = apiRouter;
