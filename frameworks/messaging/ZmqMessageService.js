const log4js = require('log4js');
const { Publisher } = require('messages');
const MessageService = require('../../application/contracts/MessageService');

log4js.configure('./config/log4js.json');
const log = log4js.getLogger('ZmqMessageService');

module.exports = class ZmqMessageServices extends MessageService {
    publish(message, topic) {
        log.info('Publishing message %s to topic %s', message, topic);
        const pub = new Publisher('tcp://127.0.0.1:3999', topic, log);
        pub.publish(message, topic);
        pub.disconnect();
    }
};
