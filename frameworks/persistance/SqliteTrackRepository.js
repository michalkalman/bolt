const sqlite = require('./SqliteHelper');
const TrackRepository = require('../../application/contracts/TrackRepository');
const Track = require('../../entities/Track');
const Activity = require('../../entities/Activity');

module.exports = class SqliteTrackRepository extends TrackRepository {
    save(trackId) {
        return new Promise((resolve, reject) => {
            try {
                const saveTrack = new Track(trackId);
                sqlite.executeSync(`INSERT INTO track (id, date) VALUES ("${saveTrack.id}",${saveTrack.date.getTime()})`);
                resolve(saveTrack);
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }

    createActivity(activityInstance) {
        return new Promise((resolve, reject) => {
            try {
                sqlite.executeSync(
                    `INSERT INTO activity (track, type, uri) VALUES ("${
                        activityInstance.track}","${activityInstance.type}","${activityInstance.uri}")`,
                );
                resolve(activityInstance);
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }

    getActivityForTrack(trackInstance) {
        return new Promise((resolve, reject) => {
            try {
                sqlite.get(`SELECT * FROM activity WHERE track='${trackInstance.id}'`)
                    .then((result) => {
                        let activity;
                        if (result) {
                            activity = new Activity(result.track, result.type, result.uri);
                        }
                        resolve(activity);
                    });
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }
};
