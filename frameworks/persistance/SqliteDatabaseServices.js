const log4js = require('log4js');
const DatabaseServices = require('../../application/contracts/DatabaseServices');
const SqliteTrackRepository = require('./SqliteTrackRepository');
const Activity = require('../../entities/Activity');
const sqlite = require('./SqliteHelper');

log4js.configure('./config/log4js.json');
const log = log4js.getLogger('SqliteDatabaseServices');

module.exports = class SqliteDatabaseServices extends DatabaseServices {
    constructor() {
        super();
        this.trackRepository = new SqliteTrackRepository();
    }

    initDatabase() {
        return new Promise((resolve, reject) => {
            try {
                this.seedData();
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    insertActivity() {
        const activities = [
            new Activity('0007684716', 'elephant', 'tcp://127.0.0.1:3999'),
            new Activity('0007669784', 'cow', 'tcp://127.0.0.1:3999'),
            new Activity('0007672247', 'chicken', 'tcp://127.0.0.1:3999'),
            new Activity('0007666484', 'bird', 'tcp://127.0.0.1:3999'),
            new Activity('0007683776', 'cat', 'tcp://127.0.0.1:3999'),
            new Activity('0007666971', 'dog', 'tcp://127.0.0.1:3999'),
            new Activity('0007662404', 'frog', 'tcp://127.0.0.1:3999'),
            new Activity('0007667477', 'pig', 'tcp://127.0.0.1:3999'),
            new Activity('0007671863', 'horse', 'tcp://127.0.0.1:3999'),
            new Activity('0007660112', 'lion', 'tcp://127.0.0.1:3999'),
            new Activity('0007668166', 'monkey', 'tcp://127.0.0.1:3999'),
            new Activity('0007665950', 'rattlesnake', 'tcp://127.0.0.1:3999'),
            new Activity('0007660406', 'rooster', 'tcp://127.0.0.1:3999'),
            new Activity('0001584773', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001584588', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0007707981', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001584227', 'playOne', 'tcp://127.0.0.1:3999'),
            new Activity('0001583884', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001601121', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001600786', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001600452', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001600124', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001599480', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001664978', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0007693000', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001602486', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001602142', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001601799', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001601457', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0001665185', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0007692643', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0007692283', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0011046750', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0012089776', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0012089835', 'playlist', 'tcp://127.0.0.1:3999'),
            new Activity('0012083659', 'playlist', 'tcp://127.0.0.1:3999'),
        ];

        activities.forEach((item) => {
            this.trackRepository.createActivity(item)
                .then((activity) => {
                    log.debug('Import %s', activity.track);
                });
        });
    }

    async seedData() {
        sqlite.get('SELECT * FROM activity')
            .then((result) => {
                if (result) {
                    log.info('No need to seed data');
                    return;
                }

                log.info('Seeding data to DB');
                sqlite.executeSync('CREATE TABLE IF NOT EXISTS track (id TEXT, date INTEGER);');
                sqlite.executeSync('CREATE TABLE IF NOT EXISTS activity (track TEXT, type TEXT, uri TEXT);');

                this.insertActivity();
            })
            .catch((err) => {
                log.error(err);
            });
    }
};
