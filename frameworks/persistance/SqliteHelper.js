const sqlite3 = require('sqlite3').verbose();

// const DB_CONNECT = '../../db/bolt';
const DB_CONNECT = './db/bolt';

const execute = (sql, params = []) => {
    const db = new sqlite3.Database(DB_CONNECT);
    return new Promise((resolve, reject) => {
        db.run(sql, params, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
        db.close();
    });
};

const get = (sql, params = []) => {
    const db = new sqlite3.Database(DB_CONNECT);
    return new Promise((resolve, reject) => {
        db.get(sql, params, (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
        db.close();
    });
};

const executeSync = (sql) => {
    const db = new sqlite3.Database(DB_CONNECT);
    db.serialize(() => {
        db.run(sql);
    });
    db.close();
};

module.exports = {
    execute,
    get,
    executeSync,
};
