const TrackRepository = require('../../application/contracts/TrackRepository');
const Track = require('../../entities/Track');

module.exports = class InMemoryTrackRepository extends TrackRepository {
    constructor() {
        super();
        this.tracks = [];
        this.activities = [];
    }

    save(trackInstance) {
        return new Promise((resolve, reject) => {
            try {
                const saveTrack = new Track(trackInstance);
                this.tracks.push(saveTrack);
                resolve(saveTrack);
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }

    createActivity(activityInstance) {
        return new Promise((resolve, reject) => {
            try {
                this.activities.push(activityInstance);
                resolve(activityInstance);
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }

    getActivityForTrack(trackInstance) {
        return new Promise((resolve, reject) => {
            try {
                const activity = this.activities.filter((x) => x.track === trackInstance.id);
                resolve(activity);
            } catch (error) {
                reject(new Error('Error Occurred'));
            }
        });
    }
};
