#!/bin/bash

if [[ $1 == "statistics" ]]
then
  printf "\nCode analyze with statistics\n"
   ./node_modules/.bin/eslint -c .eslintrc.json --ignore-path .eslintignore  ./ --format node_modules/eslint-stats/byFolder.js
   exit 0
fi

if [[ $1 == "fix" ]]
then
  printf "\nCode analyze with fix\n"
   ./node_modules/.bin/eslint -c .eslintrc.json --ignore-path .eslintignore ./ --fix
   exit 0
fi

printf "\nBasic code analyze\n"
./node_modules/.bin/eslint -c .eslintrc.json --ignore-path .eslintignore ./