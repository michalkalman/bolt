#!/bin/bash

#APP_PATH=/home/pi/bolt
APP_PATH=~/Work/morione/tp-soft/bolt

pushd $APP_PATH
	printf "\nStarting main app...\n"
	nohup npm run production > log/bolt-nohup.log &
popd

pushd $APP_PATH/external/src/theresa
    printf "\nStarting theresa app...\n"
    nohup npm run production > theresa.out &
popd

pushd $APP_PATH/external/src/player
    printf "\nStarting player app...\n"
    nohup npm run production > player.out &
popd

ps -ef|grep node