#!/bin/bash

REMOTE_USER=pi
REMOTE_IP=192.168.100.33

ssh $REMOTE_USER@$REMOTE_IP bash -s <<- __EOF
    echo "Start deploying BOLT app to 'anda' server"

    # kill all apps
    kill $(ps aux | grep 'node' | awk '{print $2}')

    PROJECT_ROOT="/home/pi"
    APP_ROOT=$PROJECT_ROOT/bolt

    if [ ! -d "$APP_ROOT" ]; then
      pushd /home/pi
        pwd
        git clone https://michalkalman@bitbucket.org/michalkalman/bolt.git
      popd
    else
      pushd /home/pi/bolt
        pwd
        git stash
        git pull origin master
      popd
    fi

    pushd /home/pi/bolt
        echo "Install BOLT app"
        chmod -R 775 *
        npm install
    popd

    pushd /home/pi/bolt/external/src/theresa
        echo "Install THERESA app"
        //TODO: check if mpg123 is installed if not then install
        npm install
    popd    

    pushd /home/pi/bolt/external/src/player
        echo "Install PLAYER app"
        //TODO: check if mpg123 is installed if not then install
        npm install
    popd  

    printf "\ninstalled bolt system\n"


	printf "Finish deploying\n"
__EOF