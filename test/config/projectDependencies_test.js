const expect = require('chai').expect;
const projectDependencies = require('../../config/projectDependencies');

describe('Project dependencies', function() {
	describe('DatabaseService', function() {
		let ds = null;

		before(function() {
			ds = projectDependencies.DatabaseService;
		});

		it('init db', function() {		
			ds.initDatabase().then((values, error) => {
				expect(values).to.be.an('undefined');
			});
		});

		it('check repository', function() {	
			expect(ds.trackRepository).to.not.be.null;
		});
	});

	describe('MessageService', function() {
		let ms = null;
		
		before(function() {
			ms = projectDependencies.MessageService;
		});

		it('publish message', function() {
			ms.publish('default', '123');
		});
	});
});
