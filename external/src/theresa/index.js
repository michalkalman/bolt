/* eslint no-undef: ["off"] */
const log4js = require('log4js');
const { exec } = require('child_process');
const { Subscriber } = require('messages');

const log = log4js.getLogger('Subscriber');

const subscriber = new Subscriber(
    'tcp://127.0.0.1:3999',
    [
        'elephant',
        'cow',
        'chicken',
        'bird',
        'pig',
        'cat',
        'dog',
        'frog',
        'goat',
        'horse',
        'lion',
        'monkey',
        'rattlesnake',
        'rooster',
    ],
    log,
);

subscriber.on('message', (obj) => {
    log.debug('receive message %s on topic %s', obj.message, obj.topic);

    const command = `mpg123 data/${obj.topic}.mp3`;
    const play = exec(command);

    play.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
});

module.exports = subscriber;
