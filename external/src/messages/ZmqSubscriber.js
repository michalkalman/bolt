const { EventEmitter } = require('events');
const zmq = require('zeromq');

module.exports = class Subscriber extends EventEmitter {
    /* eslint max-statements: ["off"] */
    constructor(address, topics, logger) {
        super();

        if (address.indexOf(':') <= 0) {
            throw new Error('socket must use a correct format!');
        }

        const self = this;
        this.logger = logger;
        this.address = address;

        // Create an array of topics.
        this.topics = Array.isArray(topics) ? topics : [topics];
        this.logger.trace('topics: %s', topics);

        this.socket = zmq.socket('sub');

        try {
            this.logger.trace('connecting sub to: %s', this.address);
            this.socket.connect(this.address);
        } catch (err) {
            this.emit('error');
            this.logger.error(err);
        }

        // Subscriptions
        if (this.topics.length > 0) {
            this.topics.forEach((topic) => {
                this.socket.subscribe(topic);
                this.logger.trace('subscribe to topic %s', topic);
            });
        } else {
            this.socket.subscribe('');
        }

        this.socket.on('message', (topic, message) => {
            self.logger.trace(message);
            self.emit('message', { topic, message });
        });
    }

    disconnect() {
        this.socket.disconnect(this.address);
        this.socket.close();
        this.emit('disconnect');
    }
};
