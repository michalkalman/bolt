const assert = require('assert');
const Publisher = require('../ZmqPublisher');

let logger =  {
		trace : function (message) {
			console.log(message);
		},
		error: function (message) {
			console.error(message);
		}
}

const pub = new Publisher('tcp://127.0.0.1:3999', 'default', logger);
pub.publish("something");
pub.disconnect();

