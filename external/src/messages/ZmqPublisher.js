const { EventEmitter } = require('events');
const zmq = require('zeromq');

module.exports = class Publisher extends EventEmitter {
    /* eslint max-statements: ["off"] */
    constructor(address, topic, logger) {
        super();

        if (address.indexOf(':') <= 0) {
            throw new Error('socket must use a correct format!');
        }

        this.logger = logger;
        this.address = address;
        this.topic = topic;
        this.logger.trace('creating publisher for topic: %s', topic);
        this.socket = zmq.socket('pub');

        try {
            this.socket.bindSync(this.address);
        } catch (err) {
            this.emit('error');
            this.logger.error(err);
        }

        // wait loop because of 0MQ init
        const wait = (ms) => {
            const start = new Date().getTime();
            let end = start;
            while (end < start + ms) {
                end = new Date().getTime();
            }
        };
        wait(500);
    }

    publish(msgBuffer, topic) {
        this.socket.send([topic, msgBuffer]);
        this.logger.trace('publishing ... %s to topic %s', msgBuffer, topic || this.topic);
    }

    disconnect() {
        this.socket.disconnect(this.address);
        this.socket.close();
        this.emit('disconnect');
    }
};
