/* eslint no-undef: ["off"] */
const log4js = require('log4js');
const fs = require('fs');
const { exec } = require('child_process');
const { Subscriber } = require('messages');

const log = log4js.getLogger('Subscriber');

const subscriber = new Subscriber(
    'tcp://127.0.0.1:3999',
    [
        'playOne',
        'playlist',
    ],
    log,
);

const readFiles = (path) => fs.readdirSync(path);

const play = (file) => {
    const command = `mpg123 ${file}`;
    const playI = exec(command);

    playI.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
};

const playlist = (path) => {
    const items = readFiles(path);
    items.forEach((f) => {
        const fileToPlay = `${path}/${f}`;
        play(fileToPlay);
    });
};

subscriber.on('message', (obj) => {
    console.log(obj.topic.toString());
    log.debug('receive message %s on topic %s', obj.message, obj.topic);

    switch (obj.topic.toString()) {
    case 'playOne':
        play(`data/playOne/${obj.message.toString()}.mp3`);
        break;
    case 'playlist':
        playlist(`data/playlist/${obj.message.toString()}`);
        break;
    default:
        console.log('default action');
        break;
    }
});

console.log('Start player app');

module.exports = subscriber;
