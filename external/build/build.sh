#!/bin/bash


#npm install npm-cache -g


# Set appropriate relative path to project root
COMPONENT_ROOT=~/Work/morione/tp-soft/bolt/external

if [ ! -d "$COMPONENT_ROOT/package" ]; then
    mkdir $COMPONENT_ROOT/package
fi

cp -R $COMPONENT_ROOT/src/* $COMPONENT_ROOT/package/ 2>&1 >/dev/null

# Install semver for symantic version comparison before anything else
pushd $COMPONENT_ROOT/package/ 2>&1 >/dev/null
    npm install semver
    npm install
popd 2>&1 >/dev/null
