const ProcessTrack = require('../application/use_cases/ProcessTrack');

module.exports = (dependecies) => {
    const { trackRepository } = dependecies.DatabaseService;
    const messageService = dependecies.MessageService;

    const processTrack = (req, res, next) => {
        const ProcessTrackCommand = ProcessTrack(trackRepository);
        ProcessTrackCommand.Execute(req.params.track)
            .then((activity) => {
                // TODO: accept more than one activity and make input validation in service
                if (activity) {
                    messageService.publish(activity[0].track, activity[0].type);
                }
                res.json({ result: 'success', activity });
            }, (err) => {
                console.error(err);
                next(err);
            });
    };

    return {
        processTrack,
    };
};
